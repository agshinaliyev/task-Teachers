package task.lesson2.teachers.Controller;


import org.springframework.web.bind.annotation.*;
import task.lesson2.teachers.Service.TeacherService;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    private void create(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
    }

    @GetMapping("/{id}")
    private Teacher get(@PathVariable Integer id) {
        return teacherService.get(id);

    }


    @DeleteMapping("/{id}")
    private void delete(@PathVariable Integer id) {
        teacherService.delete(id);


    }

//    @DeleteMapping("/{deleteAll}")
//    private void delete() {
//        teacherService.deleteAll();
//
//
//    }


    @PutMapping
    private void update(@RequestBody Teacher teacher) {
        teacherService.update(teacher);
    }


}
