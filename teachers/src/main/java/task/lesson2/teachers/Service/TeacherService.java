package task.lesson2.teachers.Service;

import org.springframework.stereotype.Service;
import task.lesson2.teachers.Controller.Teacher;
import task.lesson2.teachers.Repository.TeacherRepository;

import java.util.List;
@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;
    public TeacherService(TeacherRepository teacherRepository){

        this.teacherRepository=teacherRepository;

    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);

    }

    public void update(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public Teacher get(Integer id) {

        return teacherRepository.findById(id).get();
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }
    public void delete(Integer id){

        teacherRepository.deleteById(id);
    }
    public void deleteAll(){

        teacherRepository.deleteAll();
    }
}
