package task.lesson2.teachers.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import task.lesson2.teachers.Controller.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
}
